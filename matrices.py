import random


def makeMatrix (row,colum):
	matrix = []
	for i in range(row):
		matrix.append([])
		for j in range(colum):
			matrix[i].append(random.randrange(0, 2, 1))

	return matrix

def makeCeroMatrix (row,colum):
	matrix = []
	for i in range(row):
		matrix.append([])
		for j in range(colum):
			matrix[i].append(0)

	return matrix

def printMatrix(matrix):
	print('\n'.join([''.join(['{:4}'.format(item) for item in row]) #print matrix
      for row in matrix]))
	print('\n')




def ProcessMatrix(matrix):
	row  = len(matrix)
	colum = len(matrix[0])
	newM = makeCeroMatrix(row,colum)

	for i in range(colum):
		dfsMatrix(matrix,newM,0,i)
	for i in range(colum):
		dfsMatrix(matrix,newM,row-1,i)

	for i in range(row):
		dfsMatrix(matrix,newM,i,0)
	for i in range(row):
		dfsMatrix(matrix,newM,i,colum-1)
	printMatrix(newM)

def dfsMatrix(matrix,newM,row,colum):

	if (newM[row][colum] == 0):			#not visited
		if (matrix[row][colum] == 1):	
			newM[row][colum] = 1					#mark as visited 
			if (colum < (len(matrix[0]) -1) ):
				dfsMatrix(matrix,newM,row,colum+1)
			if (colum > 0):
				dfsMatrix(matrix,newM,row,colum-1)
			if (row > 0):
				dfsMatrix(matrix,newM,row-1,colum)
			if (row < (len(matrix) -1) ):
				dfsMatrix(matrix,newM,row+1,colum)





def main ():
	row  = int(input("Enter row number: "))
	colum = int(input("Enter column number: "))
	matrix = makeMatrix(row,colum)
	printMatrix(matrix)
	ProcessMatrix(matrix)



main()